--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE itproject;
--
-- Name: itproject; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE itproject WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE itproject OWNER TO postgres;

\connect itproject

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: itproject; Type: SCHEMA; Schema: -; Owner: itproject
--

CREATE SCHEMA itproject;


ALTER SCHEMA itproject OWNER TO itproject;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: announcements; Type: TABLE; Schema: itproject; Owner: itproject
--

CREATE TABLE itproject.announcements (
    id bigint NOT NULL,
    date timestamp with time zone NOT NULL,
    username character varying(45) NOT NULL,
    text text NOT NULL,
    project_id bigint,
    privacy bigint,
    subject character varying(100)
);


ALTER TABLE itproject.announcements OWNER TO itproject;

--
-- Name: announcements_id_seq; Type: SEQUENCE; Schema: itproject; Owner: itproject
--

CREATE SEQUENCE itproject.announcements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE itproject.announcements_id_seq OWNER TO itproject;

--
-- Name: announcements_id_seq; Type: SEQUENCE OWNED BY; Schema: itproject; Owner: itproject
--

ALTER SEQUENCE itproject.announcements_id_seq OWNED BY itproject.announcements.id;


--
-- Name: calendar; Type: TABLE; Schema: itproject; Owner: itproject
--

CREATE TABLE itproject.calendar (
    id bigint NOT NULL,
    date timestamp with time zone NOT NULL,
    username character varying(45) NOT NULL,
    project_id bigint NOT NULL,
    text character varying(120) NOT NULL,
    privacy bigint NOT NULL
);


ALTER TABLE itproject.calendar OWNER TO itproject;

--
-- Name: COLUMN calendar.privacy; Type: COMMENT; Schema: itproject; Owner: itproject
--

COMMENT ON COLUMN itproject.calendar.privacy IS '1=private
0=public';


--
-- Name: calendar_id_seq; Type: SEQUENCE; Schema: itproject; Owner: itproject
--

CREATE SEQUENCE itproject.calendar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE itproject.calendar_id_seq OWNER TO itproject;

--
-- Name: calendar_id_seq; Type: SEQUENCE OWNED BY; Schema: itproject; Owner: itproject
--

ALTER SEQUENCE itproject.calendar_id_seq OWNED BY itproject.calendar.id;


--
-- Name: chat; Type: TABLE; Schema: itproject; Owner: itproject
--

CREATE TABLE itproject.chat (
    id character varying(11) NOT NULL,
    fromuser character varying(45),
    cdate timestamp with time zone,
    message character varying(400),
    touser character varying(45),
    cread integer NOT NULL
);


ALTER TABLE itproject.chat OWNER TO itproject;

--
-- Name: feedback; Type: TABLE; Schema: itproject; Owner: itproject
--

CREATE TABLE itproject.feedback (
    id bigint NOT NULL,
    date timestamp with time zone NOT NULL,
    username character varying(45) NOT NULL,
    text text NOT NULL,
    project_id bigint,
    privacy bigint,
    subject character varying(100)
);


ALTER TABLE itproject.feedback OWNER TO itproject;

--
-- Name: feedback_id_seq; Type: SEQUENCE; Schema: itproject; Owner: itproject
--

CREATE SEQUENCE itproject.feedback_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE itproject.feedback_id_seq OWNER TO itproject;

--
-- Name: feedback_id_seq; Type: SEQUENCE OWNED BY; Schema: itproject; Owner: itproject
--

ALTER SEQUENCE itproject.feedback_id_seq OWNED BY itproject.feedback.id;


--
-- Name: files; Type: TABLE; Schema: itproject; Owner: itproject
--

CREATE TABLE itproject.files (
    name character varying(200) NOT NULL,
    username character varying(45),
    project_id bigint,
    fdate timestamp with time zone,
    privacy bigint NOT NULL,
    description character varying(120)
);


ALTER TABLE itproject.files OWNER TO itproject;

--
-- Name: COLUMN files.privacy; Type: COMMENT; Schema: itproject; Owner: itproject
--

COMMENT ON COLUMN itproject.files.privacy IS '1=private
0=public';


--
-- Name: forums; Type: TABLE; Schema: itproject; Owner: itproject
--

CREATE TABLE itproject.forums (
    id bigint NOT NULL,
    projects_id bigint NOT NULL,
    subject character varying(200) NOT NULL,
    body text NOT NULL,
    date_created timestamp with time zone,
    privacy integer,
    author character varying(120) NOT NULL
);


ALTER TABLE itproject.forums OWNER TO itproject;

--
-- Name: forums_id_seq; Type: SEQUENCE; Schema: itproject; Owner: itproject
--

CREATE SEQUENCE itproject.forums_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE itproject.forums_id_seq OWNER TO itproject;

--
-- Name: forums_id_seq; Type: SEQUENCE OWNED BY; Schema: itproject; Owner: itproject
--

ALTER SEQUENCE itproject.forums_id_seq OWNED BY itproject.forums.id;


--
-- Name: forums_messages; Type: TABLE; Schema: itproject; Owner: itproject
--

CREATE TABLE itproject.forums_messages (
    id bigint NOT NULL,
    projects_id bigint NOT NULL,
    subject character varying(200) NOT NULL,
    body text NOT NULL,
    date_created timestamp with time zone,
    privacy integer,
    author character varying(120) NOT NULL,
    forums_id bigint NOT NULL
);


ALTER TABLE itproject.forums_messages OWNER TO itproject;

--
-- Name: forums_messages_id_seq; Type: SEQUENCE; Schema: itproject; Owner: itproject
--

CREATE SEQUENCE itproject.forums_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE itproject.forums_messages_id_seq OWNER TO itproject;

--
-- Name: forums_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: itproject; Owner: itproject
--

ALTER SEQUENCE itproject.forums_messages_id_seq OWNED BY itproject.forums_messages.id;


--
-- Name: group; Type: TABLE; Schema: itproject; Owner: itproject
--

CREATE TABLE itproject."group" (
    id bigint NOT NULL,
    name character varying(45) NOT NULL,
    "desc" character varying(100)
);


ALTER TABLE itproject."group" OWNER TO itproject;

--
-- Name: group_id_seq; Type: SEQUENCE; Schema: itproject; Owner: itproject
--

CREATE SEQUENCE itproject.group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE itproject.group_id_seq OWNER TO itproject;

--
-- Name: group_id_seq; Type: SEQUENCE OWNED BY; Schema: itproject; Owner: itproject
--

ALTER SEQUENCE itproject.group_id_seq OWNED BY itproject."group".id;


--
-- Name: news; Type: TABLE; Schema: itproject; Owner: itproject
--

CREATE TABLE itproject.news (
    id bigint NOT NULL,
    date timestamp with time zone NOT NULL,
    username character varying(45) NOT NULL,
    text text NOT NULL,
    project_id bigint,
    privacy bigint,
    subject character varying(100)
);


ALTER TABLE itproject.news OWNER TO itproject;

--
-- Name: news_id_seq; Type: SEQUENCE; Schema: itproject; Owner: itproject
--

CREATE SEQUENCE itproject.news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE itproject.news_id_seq OWNER TO itproject;

--
-- Name: news_id_seq; Type: SEQUENCE OWNED BY; Schema: itproject; Owner: itproject
--

ALTER SEQUENCE itproject.news_id_seq OWNED BY itproject.news.id;


--
-- Name: position; Type: TABLE; Schema: itproject; Owner: itproject
--

CREATE TABLE itproject."position" (
    id bigint NOT NULL,
    group_id bigint NOT NULL,
    name character varying(45) NOT NULL,
    "desc" character varying(100)
);


ALTER TABLE itproject."position" OWNER TO itproject;

--
-- Name: position_id_seq; Type: SEQUENCE; Schema: itproject; Owner: itproject
--

CREATE SEQUENCE itproject.position_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE itproject.position_id_seq OWNER TO itproject;

--
-- Name: position_id_seq; Type: SEQUENCE OWNED BY; Schema: itproject; Owner: itproject
--

ALTER SEQUENCE itproject.position_id_seq OWNED BY itproject."position".id;


--
-- Name: projects; Type: TABLE; Schema: itproject; Owner: itproject
--

CREATE TABLE itproject.projects (
    id bigint NOT NULL,
    name character varying(120) NOT NULL,
    manager character varying(45) NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    actual_start_date date,
    actual_end_date date,
    status bigint NOT NULL,
    privacy bigint NOT NULL,
    description text
);


ALTER TABLE itproject.projects OWNER TO itproject;

--
-- Name: COLUMN projects.status; Type: COMMENT; Schema: itproject; Owner: itproject
--

COMMENT ON COLUMN itproject.projects.status IS 'done=1
ongoing=2
on hold=3
cancelled=4
';


--
-- Name: COLUMN projects.privacy; Type: COMMENT; Schema: itproject; Owner: itproject
--

COMMENT ON COLUMN itproject.projects.privacy IS '0=private
1=public';


--
-- Name: projects_id_seq; Type: SEQUENCE; Schema: itproject; Owner: itproject
--

CREATE SEQUENCE itproject.projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE itproject.projects_id_seq OWNER TO itproject;

--
-- Name: projects_id_seq; Type: SEQUENCE OWNED BY; Schema: itproject; Owner: itproject
--

ALTER SEQUENCE itproject.projects_id_seq OWNED BY itproject.projects.id;


--
-- Name: projects_members; Type: TABLE; Schema: itproject; Owner: itproject
--

CREATE TABLE itproject.projects_members (
    project_id bigint NOT NULL,
    username character varying(45)
);


ALTER TABLE itproject.projects_members OWNER TO itproject;

--
-- Name: users; Type: TABLE; Schema: itproject; Owner: itproject
--

CREATE TABLE itproject.users (
    username character varying(45) NOT NULL,
    firstname character varying(45) NOT NULL,
    lastname character varying(45) NOT NULL,
    email character varying(120) NOT NULL,
    position_id bigint NOT NULL,
    group_id bigint NOT NULL,
    password character varying(12) NOT NULL,
    online bigint NOT NULL
);


ALTER TABLE itproject.users OWNER TO itproject;

--
-- Name: COLUMN users.position_id; Type: COMMENT; Schema: itproject; Owner: itproject
--

COMMENT ON COLUMN itproject.users.position_id IS '0 = staff ; 1 = manager';


--
-- Name: announcements id; Type: DEFAULT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject.announcements ALTER COLUMN id SET DEFAULT nextval('itproject.announcements_id_seq'::regclass);


--
-- Name: calendar id; Type: DEFAULT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject.calendar ALTER COLUMN id SET DEFAULT nextval('itproject.calendar_id_seq'::regclass);


--
-- Name: feedback id; Type: DEFAULT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject.feedback ALTER COLUMN id SET DEFAULT nextval('itproject.feedback_id_seq'::regclass);


--
-- Name: forums id; Type: DEFAULT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject.forums ALTER COLUMN id SET DEFAULT nextval('itproject.forums_id_seq'::regclass);


--
-- Name: forums_messages id; Type: DEFAULT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject.forums_messages ALTER COLUMN id SET DEFAULT nextval('itproject.forums_messages_id_seq'::regclass);


--
-- Name: group id; Type: DEFAULT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject."group" ALTER COLUMN id SET DEFAULT nextval('itproject.group_id_seq'::regclass);


--
-- Name: news id; Type: DEFAULT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject.news ALTER COLUMN id SET DEFAULT nextval('itproject.news_id_seq'::regclass);


--
-- Name: position id; Type: DEFAULT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject."position" ALTER COLUMN id SET DEFAULT nextval('itproject.position_id_seq'::regclass);


--
-- Name: projects id; Type: DEFAULT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject.projects ALTER COLUMN id SET DEFAULT nextval('itproject.projects_id_seq'::regclass);


--
-- Data for Name: announcements; Type: TABLE DATA; Schema: itproject; Owner: itproject
--

COPY itproject.announcements (id, date, username, text, project_id, privacy, subject) FROM stdin;
13	2010-07-09 10:13:51+00	jinky	We will have a general assembly meeting at the 33rd floor	0	1	General Assembly
\.


--
-- Data for Name: calendar; Type: TABLE DATA; Schema: itproject; Owner: itproject
--

COPY itproject.calendar (id, date, username, project_id, text, privacy) FROM stdin;
20	2010-07-20 23:30:00+00	jsanjuan	0	test 11:30pm	1
\.


--
-- Data for Name: chat; Type: TABLE DATA; Schema: itproject; Owner: itproject
--

COPY itproject.chat (id, fromuser, cdate, message, touser, cread) FROM stdin;
\.


--
-- Data for Name: feedback; Type: TABLE DATA; Schema: itproject; Owner: itproject
--

COPY itproject.feedback (id, date, username, text, project_id, privacy, subject) FROM stdin;
\.


--
-- Data for Name: files; Type: TABLE DATA; Schema: itproject; Owner: itproject
--

COPY itproject.files (name, username, project_id, fdate, privacy, description) FROM stdin;
sorting.doc	jinky	1	2010-07-09 10:00:38+00	1	test document
\.


--
-- Data for Name: forums; Type: TABLE DATA; Schema: itproject; Owner: itproject
--

COPY itproject.forums (id, projects_id, subject, body, date_created, privacy, author) FROM stdin;
7	0	This is the first Topic	Topic Body	2010-07-05 00:00:00+00	1	jsanjuan
8	0	this is the second topic	testing of the 2nd topic	2010-07-06 00:00:00+00	1	jsanjuan
\.


--
-- Data for Name: forums_messages; Type: TABLE DATA; Schema: itproject; Owner: itproject
--

COPY itproject.forums_messages (id, projects_id, subject, body, date_created, privacy, author, forums_id) FROM stdin;
4	0		this is a response to 1st	2010-07-05 00:00:00+00	1	jsanjuan	7
5	0		another response	2010-07-05 00:00:00+00	1	jsanjuan	7
\.


--
-- Data for Name: group; Type: TABLE DATA; Schema: itproject; Owner: itproject
--

COPY itproject."group" (id, name, "desc") FROM stdin;
1	default	default group
2	Office Administration	Office Administration
3	HR	HR
4	Sales	Sales
5	Software Development	Software Development
6	Network	Network
7	Systems	Systems
8	DevOps	DevOps
9	Project Management	Project Management
\.


--
-- Data for Name: news; Type: TABLE DATA; Schema: itproject; Owner: itproject
--

COPY itproject.news (id, date, username, text, project_id, privacy, subject) FROM stdin;
19	2010-07-26 14:48:09+00	jinky	test	0	1	test
\.


--
-- Data for Name: position; Type: TABLE DATA; Schema: itproject; Owner: itproject
--

COPY itproject."position" (id, group_id, name, "desc") FROM stdin;
1	0	Administrator	Admin
2	5	Java Developer	Java Developer
3	5	PHP Developer	PHP Developer
4	5	Golang Developer	Golang Developer
5	5	Python Developer	Python Developer
6	5	C++ Developer	C++ Developer
7	7	Windows Systems Administrator	Windows Systems Administrator
8	7	AIX Systems Administrator	AIX Systems Administrator
9	7	Solaris Systems Administrator	Solaris Systems Administrator
10	7	Linux Systems Administrator	Linux Systems Administrator
11	9	Account Manager 1	Account Manager 1
12	9	Account Manager 2	Account Manager 2
13	9	Account Manager 3	Account Manager 3
16	9	Senior Account Manager 1	Senior Account Manager 1
17	9	Senior Account Manager 2	Senior Account Manager 2
18	9	Senior Account Manager 3	Senior Account Manager 3
19	9	Project Manager 1	Project Manager 1
20	9	Project Manager 2	Project Manager 2
21	9	Project Manager 3	Project Manager 3
22	3	Recruiter 1	Recruiter 1
23	3	Recruiter 2	Recruiter 2
24	3	Recruiter 3	Recruiter 3
25	3	Recruiter 4	Recruiter 4
26	3	Recruiter 5	Recruiter 5
27	6	Network Engineer 1	Network Engineer 1
28	6	Network Engineer 2	Network Engineer 2
29	6	Network Engineer 3	Network Engineer 3
30	6	Network Engineer 4	Network Engineer 4
31	6	Senior Network Engineer 1	Senior Network Engineer 1
32	6	Senior Network Engineer 2	Senior Network Engineer 2
33	8	DevOps 1	DevOps 1
34	8	DevOps 2	DevOps 2
35	4	Sales Executive 1	Sales Executive 1
36	4	Sales Executive 2	Sales Executive 2
37	4	Sales Executive 3	Sales Executive 3
38	4	Senior Sales Executive 1	Senior Sales Executive 1
39	4	Senior Sales Executive 2	Senior Sales Executive 2
40	3	HR Executive 1	HR Executive 1
41	3	HR Executive 2	HR Executive 2
42	3	HR Manager	HR Manager
43	7	Systems Manager	Systems Manager
44	6	Network Manager	Network Manager
45	8	DevOps Manager	DevOps Manager
46	4	Sales Manager	Sales Manager
47	2	Office Manager	Office Manager
48	2	Office Assistant Manager	Office Assistant Manager
49	5	Software Development Manager	Software Development Manager
\.


--
-- Data for Name: projects; Type: TABLE DATA; Schema: itproject; Owner: itproject
--

COPY itproject.projects (id, name, manager, start_date, end_date, actual_start_date, actual_end_date, status, privacy, description) FROM stdin;
53	test	jsanjuan	2010-01-01	2010-08-04	2010-01-01	2010-08-04	2	1	test
51	Online Registration Project	jsanjuan	2010-01-01	2010-07-14	2010-01-01	2010-07-14	2	1	Online registration project for a cinema
\.


--
-- Data for Name: projects_members; Type: TABLE DATA; Schema: itproject; Owner: itproject
--

COPY itproject.projects_members (project_id, username) FROM stdin;
49	jsanjuan
52	miket
51	jinky
53	jinky
54	jsanjuan
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: itproject; Owner: itproject
--

COPY itproject.users (username, firstname, lastname, email, position_id, group_id, password, online) FROM stdin;
administrator	Administrator		admin@itproject-portal.com	1	0	password	0
jsanjuan	Joshua	San Juan	jsanjuan@test.com	0	0	password	0
\.


--
-- Name: announcements_id_seq; Type: SEQUENCE SET; Schema: itproject; Owner: itproject
--

SELECT pg_catalog.setval('itproject.announcements_id_seq', 13, true);


--
-- Name: calendar_id_seq; Type: SEQUENCE SET; Schema: itproject; Owner: itproject
--

SELECT pg_catalog.setval('itproject.calendar_id_seq', 20, true);


--
-- Name: feedback_id_seq; Type: SEQUENCE SET; Schema: itproject; Owner: itproject
--

SELECT pg_catalog.setval('itproject.feedback_id_seq', 1, true);


--
-- Name: forums_id_seq; Type: SEQUENCE SET; Schema: itproject; Owner: itproject
--

SELECT pg_catalog.setval('itproject.forums_id_seq', 8, true);


--
-- Name: forums_messages_id_seq; Type: SEQUENCE SET; Schema: itproject; Owner: itproject
--

SELECT pg_catalog.setval('itproject.forums_messages_id_seq', 5, true);


--
-- Name: group_id_seq; Type: SEQUENCE SET; Schema: itproject; Owner: itproject
--

SELECT pg_catalog.setval('itproject.group_id_seq', 9, true);


--
-- Name: news_id_seq; Type: SEQUENCE SET; Schema: itproject; Owner: itproject
--

SELECT pg_catalog.setval('itproject.news_id_seq', 19, true);


--
-- Name: position_id_seq; Type: SEQUENCE SET; Schema: itproject; Owner: itproject
--

SELECT pg_catalog.setval('itproject.position_id_seq', 49, true);


--
-- Name: projects_id_seq; Type: SEQUENCE SET; Schema: itproject; Owner: itproject
--

SELECT pg_catalog.setval('itproject.projects_id_seq', 53, true);


--
-- Name: announcements idx_16389_primary; Type: CONSTRAINT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject.announcements
    ADD CONSTRAINT idx_16389_primary PRIMARY KEY (id);


--
-- Name: calendar idx_16398_primary; Type: CONSTRAINT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject.calendar
    ADD CONSTRAINT idx_16398_primary PRIMARY KEY (id);


--
-- Name: feedback idx_16410_primary; Type: CONSTRAINT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject.feedback
    ADD CONSTRAINT idx_16410_primary PRIMARY KEY (id);


--
-- Name: files idx_16417_primary; Type: CONSTRAINT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject.files
    ADD CONSTRAINT idx_16417_primary PRIMARY KEY (name);


--
-- Name: forums idx_16422_primary; Type: CONSTRAINT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject.forums
    ADD CONSTRAINT idx_16422_primary PRIMARY KEY (id);


--
-- Name: forums_messages idx_16431_primary; Type: CONSTRAINT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject.forums_messages
    ADD CONSTRAINT idx_16431_primary PRIMARY KEY (id);


--
-- Name: group idx_16440_primary; Type: CONSTRAINT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject."group"
    ADD CONSTRAINT idx_16440_primary PRIMARY KEY (id);


--
-- Name: news idx_16446_primary; Type: CONSTRAINT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject.news
    ADD CONSTRAINT idx_16446_primary PRIMARY KEY (id);


--
-- Name: position idx_16455_primary; Type: CONSTRAINT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject."position"
    ADD CONSTRAINT idx_16455_primary PRIMARY KEY (id);


--
-- Name: projects idx_16461_primary; Type: CONSTRAINT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject.projects
    ADD CONSTRAINT idx_16461_primary PRIMARY KEY (id);


--
-- Name: users idx_16471_primary; Type: CONSTRAINT; Schema: itproject; Owner: itproject
--

ALTER TABLE ONLY itproject.users
    ADD CONSTRAINT idx_16471_primary PRIMARY KEY (username);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

