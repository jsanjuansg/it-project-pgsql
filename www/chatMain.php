<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$username = $_SESSION['itp_username'];

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Calendar Main</title>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain"><img src="img/chat.gif" width="48" height="48"> Chat</td>
        </tr>
        <tr class="maintext">
          <td width="21%">&nbsp;</td>
        </tr>
        
        <tr class="maintext">
          <td><table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
            <tr>
              <td width="10%" class="menubar">Online</td>
              <td width="20%" class="menubar">&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
            

<?php

$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 

/*

id
date
username
project_id
text
privacy


*/
$sql = sprintf("SELECT username,firstname,lastname FROM users WHERE online = 1 AND username <> '%s'", $username);


//echo $sql;


$result = pg_query($conn, $sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

while ( $row = pg_fetch_assoc($result) ) {
	echo "<tr>\n";
	
	echo "<td>\n";
	echo "<a href=\"chatFrame.php?to=";
	echo $row['username'];
	echo "\">";
	echo $row['username'];
	echo "</a>";
	echo "</td>\n";
	
	echo "<td>\n";
	echo $row['firstname'];
	echo "&nbsp;";
	echo $row['lastname'];
	echo "</td>\n";
	
	echo "</tr>\n";
}

 
pg_close($conn);


?>
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </tr>
</table>
</body>
</html>
