<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Project: Create New</title>
<style type="text/css">
<!--
.style37 {font-family: Arial, Helvetica, sans-serif; font-size: 14; }
.style38 {font-size: 14}
-->
</style>
</head>

<body>
<form name="form1" method="post" action="projectsDoCreate.php">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Projects Create New</td>
        </tr>
        <tr class="maintext">
          <td width="21%">&nbsp;</td>
        </tr>
        
        <tr class="maintext">
          <td><table width="70%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#CCCCCC" class="dialogbox">
            <tr>
              <td width="50%" class="style37"><p class="style37">Project Name: *</p></td>
              <td width="50%"><span class="style38">
                <label>
                <input type="text" name="projectname" id="projectname" />
                </label>
              </span></td>
            </tr>
            <tr>
              <td class="style37">Project Description:</td>
              <td><span class="style38">
                <label>
                <textarea name="projectdescription" id="projectdescription" cols="45" rows="5" tabindex="2"></textarea>
                </label>
              </span></td>
            </tr>
            <tr>
              <td class="style37">Project Start Date: *</td>
              <td>

<?php


echo GenerateSelectMonths("smonth",1);
echo GenerateSelectDays("sday", 1, 31, 1);
echo GenerateSelectYears("syear", 2010, 2015, 2010);



?>              </td>
            </tr>
            <tr>
              <td class="style37">Project End Date: *</td>
              <td>

<?php


echo GenerateSelectMonths("emonth",1);
echo GenerateSelectDays("eday", 1, 31, 1);
echo GenerateSelectYears("eyear", 2010, 2015, 2010);



?>              </td>
            </tr>
            
            <tr>
              <td class="style37"><span class="style38"></span></td>
              <td><span class="style38"></span></td>
            </tr>
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">Project Members: *</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            
            
<?php

$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}



/*

username
firstname
lastname
email
position_id
group_id
password

*/

$sql = "SELECT username,firstname,lastname FROM users";

//echo $sql;


$result = pg_query($conn, $sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $query;
    die($message);
}

while ( $row = pg_fetch_assoc($result) ) {
	if ( $row['username'] != "administrator" )  {
		echo "<tr>\n";
		echo "<td colspan=\"2\">\n";
		echo "<input type=\"checkbox\" name=\"projectmembers[]\" value=\"" . $row['username'] . "\">\n";
		echo $row['firstname'] . " " . $row['lastname'];	
		echo "</td>\n";
		echo "</tr>\n";
	}
}

 
pg_close($conn);


?>



            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">* required fields</td>
            </tr>
            <tr>
              <td colspan="2" class="style37"><label>
                  <div align="center">
                    <input name="createproject" type="submit" class="dialogboxbuttons" id="createproject" value="     CREATE PROJECT     " />
                  </div>
                </label></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
  </tr>
</table>
</form>
</body>
</html>
