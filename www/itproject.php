<?php


//  application

$apptitlebar = "IT PROJECTS PORTAL";
$itproject_url = "";

//  database

$dbhost = "host = pgsql";
$dbport = "port = 5432";
$dbase = "dbname = itproject";
$dbuser = "user = itproject";
$dbpassword = "password=Db#sap1434";


//  date functions

$itp_months = array("", "January", "February", "March", 
                    "April", "May", "June", "July", 
                    "August", "September", "October", 
                    "November", "December");

$itp_months_days= array(0, 31, 28, 31, 30, 31, 30, 31, 
                        31, 30, 31, 30, 31); 


function check_leap_year($t)  {
	if ( date("L", $t) )  {
		$itp_months_days[2] = 29;
	}
}


/*
 *  generate an HTML <select> element containing
 *  the 12 months
 *
 */
function GenerateSelectMonths($name, $selected)  {

	$itp_months = array("", "January", "February", "March", 
                    "April", "May", "June", "July", 
                    "August", "September", "October", 
                    "November", "December");
					
    $retval = "<select name=\"".$name."\">";
    for ( $i = 1; $i <= 12; ++$i )  {
        if ( $i == $selected )  {
            $retval .= "<option value=\"".$i."\" selected>".$itp_months[$i]."</option>";
        }  else  {
            $retval .= "<option value=\"".$i."\">".$itp_months[$i]."</option>";
        }
    }
    $retval .= "</select>";
      
    return $retval;

}


/*
 *  generate an HTML <select> element containing
 *  the years from $syear to $eyear
 *
 */
function GenerateSelectYears($name, $syear, $eyear, $selected)  {

	for ( $i = $syear; $i <= $eyear; ++$i )  {
    	$yrs[$i] = $i;
    }

    $retval = "<select name=\"".$name."\">";
    foreach ( $yrs as $key => $val )  {
        if ( $key == $selected )  {
            $retval .= "<option value=\"".$key."\" selected>".$val."</option>";  
        }  else  {
            $retval .= "<option value=\"".$key."\">".$val."</option>";
        }
    }
    $retval .= "</select>";
  
    return $retval;

}


/*
 *  generate an HTML <select> element containing
 *  days starting from $sdate up to $edate
 *
 */
function GenerateSelectDays($name, $sdate, $edate, $selected)  {

    $retval = "<select name=\"".$name."\">";
    for ( $i = $sdate; $i <= $edate; ++$i )  {
        if ( $i == $selected )  {
           $retval .= "<option value=\"".$i."\" selected>".$i."</option>";  
        }  else  {
           $retval .= "<option value=\"".$i."\">".$i."</option>";
        }
    }
    $retval .= "</select>";
    
    return $retval;
	
}

/*
 *  return duration from $from_date to $to_date
 *  $from_date, $to_date must be timestamps
 *
 */
function time_duration($from_date, $to_date)  {

	$dur_allsec = $to_date - $from_date;
    $dur_allmin = $dur_allsec / 60;
    $dur_allhr = $dur_allmin / 60;
    $dur_allday = $dur_allhr / 24;
    
    //  duration in seconds only
    //
    if ( $dur_allmin < 1 )  {
        $dur_summary = $dur_allsec." secs";

    //  duration took minutes
    //
    }  else if ( $dur_allhr < 1 )  {
           
        $desecs = $dur_allsec % 60;
        if ( $desecs > 0 )  {
            $dmin = (int) $dur_allmin;
            $dur_summary = $dmin." mins";
            $dur_summary .= ", ".$desecs." secs";
         }  else  {
            $dur_summary = $dur_allmin." mins";
         }  
    
    //  duration took hours
    //
    }  else if ( $dur_allday < 1 )  {
        $dur_summary = $dur_allhr." hrs";

    //  duration took days
    //
    }  else  {
        $dur_summary = $dur_allday." days";
    }

    $retval["allsec"] = $dur_allsec;
    $retval["allmin"] = $dur_allmin;
    $retval["allhr"] = $dur_allhr;
    $retval["allday"] = $dur_allday;
    $retval["summary"] = $dur_summary;

    return $retval;

}


?>
