<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Profiles Add New</title>
</head>

<body>
<form name="form1" method="post" action="profilesDoAdd.php">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>     
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Add New Profile</td>
        </tr>
        <tr class="maintext">
          <td width="24%">Username: *</td>
          <td width="76%">
            <label>
              <input type="text" name="username" id="username">
              </label>          </td>
        </tr>  
         <tr class="maintext">
          <td width="24%">First Name: *</td>
          <td width="76%"><input type="text" name="firstname" id="firstname"></td>
        </tr>      
         <tr class="maintext">
           <td>Last Name: *</td>
           <td><input type="text" name="lastname" id="lastname"></td>
         </tr>
        <tr class="maintext">
          <td width="24%"> Password: *</td>
          <td width="76%"><input type="password" name="password1" id="password1"></td>
        </tr>      
        <tr class="maintext">
          <td width="24%">Re-type Password: *</td>
          <td width="76%"><input type="password" name="password2" id="password2"></td>
        </tr>
        <tr class="maintext">
          <td>E-mail address: *</td>
          <td><input type="text" name="email" id="email"></td>
        </tr>
        <tr class="maintext">
          <td>Role: *</td>
          <td><label>
            <input name="role" type="radio" id="radio" value="0" checked>
            Staff 
            <input type="radio" name="role" id="radio2" value="1">
            Manager
          </label></td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td>* required field</td>
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td colspan="2"><input type="submit" name="submit" id="submit" value="     Add New Profile     "></td>
          </tr>      
      </table>
  </tr>
</table>
</form>
</body>
</html>
