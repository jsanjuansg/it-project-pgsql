<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Profiles Main</title>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain"> <img src="img/profile.gif" width="48" height="48"> My Profile</td>
        </tr>
        <tr class="maintext">
          <td width="21%"><a href="profilesChangeUserPass.php">Change My Password</a></td>
        </tr>
        <tr class="maintext">
          <td><a href="profilesModifyUserProfile.php">Modify My Profile</a></td>
        </tr>
      </table>
      <p>&nbsp;</p>


<?php

if ( $_SESSION['itp_username'] == "administrator" )  {

print <<< END
      
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Profiles</td>
        </tr>
        <tr class="maintext">
          <td width="22%"><a href="profilesAdd.php">Add New Profile</a></td>
        </tr>      
        <tr class="maintext">
          <td width="22%">Change Password</td>
        </tr>      
        <tr class="maintext">
          <td width="22%">Modify Profile</td>
        </tr>      
      </table>

END;

}


?>
      
      
  </tr>
</table>
</body>
</html>
