<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: News Main</title>
</head>

<body>
<form name="form1" method="post" action="filesDoDelete1.php">

<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain"><img src="img/files.gif" width="32" height="32">Files</td>
        </tr>
        <tr class="maintext">
          <td width="21%"><a href="filesUploadNew.php">Upload File</a></td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
            <tr>
              <td width="6%" class="menubar">&nbsp;</td>
              <td width="44%" class="menubar">Filename</td>
              <td width="29%" class="menubar">Description</td>
              <td width="21%" class="menubar">Date</td>
            </tr>
            <tr>
            

<?php

$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 

/*

name
username
project_id
fdate
privacy
description

*/
$sql = "SELECT name,fdate,description FROM files";

//echo $sql;


$result = pg_query($conn, $sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $query;
    die($message);
}

while ( $row = pg_fetch_assoc($result) ) {
	echo "<tr>\n";
	
	echo "<td>";
	echo "<input type=\"checkbox\" name=\"fileselect[]\" value=\"";
	echo $row['name'];
	echo "\">";
	echo "</td>";
	
	echo "<td>\n";
	$file_url = "<a href=\"" . $itproject_url . "/";
	$file_url .= "files/" . $row['name'];
	$file_url .= "\">";
	echo $file_url;
	echo $row['name'];
	echo "</a>";
	echo "</td>\n";
	
	echo "<td>";
	echo $row['description'];
	
	echo "</td>";
	echo "<td>\n";
	echo $row['fdate'];
	echo "</td>\n";
	
	echo "</tr>\n";
}

 
pg_close($conn);


?>
            </tr>
            <tr>
              <td width="6%" class="menubar">&nbsp;</td>
              <td width="44%" class="menubar">&nbsp;</td>
              <td width="29%" class="menubar"><input name="delete" type="submit" class="dialogboxbuttons" value="    DELETE SELECTED FILES    "></td>
              <td width="21%" class="menubar">&nbsp;</td>
            </tr>
            
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </tr>
</table>
</form>
</body>
</html>
