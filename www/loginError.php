<html>

<?php

require_once "itproject.php";

?>

<head>
<title>IT Project: Login</title>
<link rel="stylesheet" href="itproject.css" type="text/css">
<script language="JavaScript" src="itproject.js">
</script>
</head>

<!--
1000 milliseconds in one second for setTimeout
-->

<body onLoad="setTimeout('itredirect()', 5000)">
<table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr class="logintitlebar">
    <td height="56">
    
<?php

echo $apptitlebar;

?>       
    
    </td>
    <td><span class="logindate">Today is 

<?php

	echo date('D F, j, Y');

?>    
    
    </span>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <p>&nbsp;</p>
      <table width="40%" border="0" align="center" cellpadding="2" cellspacing="0" class="loginerrorsubtitlebar">
      <tr>
        <td><table width="100%" border="0" align="center" cellpadding="10" cellspacing="0">
          <tr>
            <td colspan="2" class="loginerrorsubtitlebar">Login Error</td>
            </tr>
          <tr class="loginsubtitlecontent">
            <td colspan="2">
            Invalid username and/or password.  Please try to login again.  You will be redirected to the login page in 5 seconds or you can click <a href="index.php">here</a> to go back to the login page.            
            </td>
          </tr>        
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
