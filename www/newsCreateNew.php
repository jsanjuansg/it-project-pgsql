<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
	exit;
}

if ( $_SESSION['itp_position'] == 0 )  {						
	header('Location: index.phpnewsMain.php');
	exit;          

}  else if ( $_SESSION['itp_username'] != "administrator" &&  $_SESSION['itp_position'] != 1 )  {
	header('Location: index.phpnewsMain.php');
	exit;          
}


?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: News Main</title>
<style type="text/css">
<!--
.style37 {font-family: Arial, Helvetica, sans-serif; font-size: 14; }
.style38 {font-size: 14}
-->
</style>
</head>

<body>
<form name="form1" method="post" action="newsDoCreate.php">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Create News</td>
        </tr>
        <tr class="maintext">
          <td width="21%">&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="70%" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#CCCCCC" class="dialogbox">
            <tr>
              <td width="50%" class="style37"><p class="style37">Subject: *</p></td>
              <td width="50%"><span class="style38">
                <label>
                <input type="text" name="newssubject">
                </label>
              </span></td>
            </tr>
            <tr>
              <td class="style37">Body:</td>
              <td><span class="style38">
                <label>
                <textarea name="newsbody" cols="45" rows="5" tabindex="2"></textarea>
                </label>
              </span></td>
            </tr>
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">* required fields</td>
            </tr>
            <tr>
              <td colspan="2" class="style37"><label>
                  <div align="center">
                    <input name="createproject" type="submit" class="dialogboxbuttons" id="createproject" value="     CREATE NEWS     " />
                  </div>
                </label></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
  </tr>
</table>
</form>
</body>
</html>
