<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}


$id = $_POST['id'];

if ( empty($id) )  {
	header('Location: index.phpforumsMain.php');
	exit;
}


$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use DB : ' . $conn->error);
}

$sql2 = sprintf("DELETE FROM forums_messages WHERE id = %d", $id);

//echo $sql1;

$result = pg_query($conn, $sql2);
if (!$result2) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql2;
    die($message);
}


pg_close($conn);


?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Forums Main</title>
<script language="JavaScript" src="itproject.js">
</script>
</head>

<body onLoad="setTimeout('forumsredirect()', 3000)">

<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Forum Post Deleted</td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td>You will now be redirected to the Forums Main Page in 3 seconds. If not, click <a href="forumsMain.php">here</a> </td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </tr>
</table>

</body>
</html>
