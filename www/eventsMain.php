<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Events Main</title>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain"><img src="img/events.gif" width="32" height="32"> Events</td>
        </tr>
        <tr class="maintext">
          <td width="21%"><a href="eventsCreateNew.php">Create New Event</a></td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
            <tr>
              <td width="10%" class="menubar">Date/Time</td>
              <td width="20%" class="menubar">Subject</td>
            </tr>
            <tr>
            

<?php

$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 


/*

id
date
username
project_id
text
privacy


*/
$sql = "SELECT id,date,text FROM calendar";

//echo $sql;


$result = pg_query($conn, $sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $query;
    die($message);
}

while ( $row = pg_fetch_assoc($result) ) {
	echo "<tr>\n";
	
	echo "<td>\n";
	echo $row['date'];
	echo "</td>\n";
	
	echo "<td>\n";
	$event_url = "<a href=\"" . $itproject_url . "/";
	$event_url .= "eventsView.php?id=" . $row['id'];
	$event_url .= "\">";
	echo $event_url;
	echo $row['text'];
	echo "</a>";
	echo "</td>\n";


	echo "</tr>\n";
}

 
pg_close($conn);


?>

            
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </tr>
</table>
</body>
</html>
