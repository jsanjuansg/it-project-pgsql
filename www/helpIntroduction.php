<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Help Main</title>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Help</td>
        </tr>
        <tr class="maintext">
          <td width="21%"></td>
        </tr>
        <tr class="maintext">
          <td><a href="helpMain.php">Help Main Page</a></td>
        </tr>
        <tr class="maintext">
          <td><table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
            <tr>
              <td width="44%" class="menubar">Introduction</td>             
            </tr>
            <tr>
              <td width="44%" class="maintext">This is the introduction</td>
            </tr>                                
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </tr>
</table>
</body>
</html>
