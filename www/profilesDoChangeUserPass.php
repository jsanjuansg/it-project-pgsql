<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$username = $_SESSION['itp_username'];
$oldpassword = trim($_POST['oldpassword']);
$password1 = trim($_POST['password1']);
$password2 = trim($_POST['password2']);

if ( empty($username) )  {
	header('Location: index.phperrorFieldsMissing.php');
}

if ( empty($oldpassword) )  {
	header('Location: index.phperrorFieldsMissing.php');
}

if ( $password1 != $password2 )  {
	header('Location: index.phperrorPasswordNotMatch.php');
}

$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}

$username = pg_escape_string($username);
$oldpassword = pg_escape_string($oldpassword);
$password1 = pg_escape_string($password1);
$password2 = pg_escape_string($password2);

$sql = sprintf("SELECT password FROM users WHERE username='%s'", $username);


$result = pg_query($conn, $sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}
$row = pg_fetch_assoc($result);
$epass = $row['password'];

if ( $oldpassword != $epass )  {
	header('Location: index.phperrorIncorrectPassword.php');
}


/*

username
firstname
lastname
email
position_id
group_id
password
online

*/

$sql = sprintf("UPDATE users SET password = '%s' WHERE username = '%s'",$password1,$username); 

//echo $sql;


$result = pg_query($conn, $sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

pg_close($conn);


?>
<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Profiles Added</title>
<script language="JavaScript" src="itproject.js">
</script>
</head>

<body onLoad="setTimeout('profilesredirect()', 3000)">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>     
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Your Password Has Been Changed</td>
        </tr>
        <tr class="maintext">
          <td width="24%">Username: </td>
          <td width="76%">
<?php

	echo $username;

?>          
          
          </td>
        </tr>  
        <tr class="maintext">
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td colspan="2">You will now be redirected to the Profiles Page in 3 seconds. If not, click <a href="profilesMain.php">here</a> </td>
        </tr>      
      </table>
  </tr>
</table>
</form>
</body>
</html>