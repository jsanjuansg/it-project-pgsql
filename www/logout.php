<?php

require_once "itproject.php";

session_start();

if ( isset($_SESSION['itp_username']) )  {

	$user = $_SESSION['itp_username'];
	unset ($_SESSION['itp_username']);
	unset ($_SESSION['itp_position']);
	session_destroy();
	
	$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
	if (!$conn) {
    	die('Could not connect: ' . $conn->error);
	}


 	$sql2 = sprintf("UPDATE users SET online = 0 WHERE username='%s'",
            pg_escape_string($user));

	$result2 = pg_query($conn, $sql2);
	if (!$result2) {
    	$message  = 'Invalid query: ' . $conn->error . "\n";
    	$message .= 'Whole query: ' . $sql2;
    	die($message);
	}
		
	pg_close($conn);
		
	header('Location: index.php');
	
}  else  {
	session_destroy();
	header('Location: index.php');
}

?>
