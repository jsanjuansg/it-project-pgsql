<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: News Main</title>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain"><img src="img/news.gif" width="32" height="32"> News</td>
        </tr>
        <tr class="maintext">
          <td width="21%">
          
<?php


if ( $_SESSION['itp_position'] == 1 )  {          
	echo "<a href=\"newsCreateNew.php\">Create News</a>";
	
}  elseif ( $_SESSION['itp_username'] == "administrator" )  {
	echo "<a href=\"newsCreateNew.php\">Create News</a>";
	
}  else  {
	echo "&nbsp;";
}
		  
?>
		  
		  </td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
            <tr>
              <td width="44%" class="menubar">Date</td>
              <td width="20%" class="menubar">Subject</td>
            </tr>
            <tr>
            

<?php

$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 
 


/*

id
date
username
text
project_id
privacy
subject

*/

$sql = "SELECT id,date,subject FROM news";

//echo $sql;


$result = pg_query($conn, $sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $query;
    die($message);
}

while ( $row = pg_fetch_assoc($result) ) {
	echo "<tr>\n";
	
	
	echo "<td>\n";
	echo $row['date'];
	echo "</td>\n";
	
	echo "<td>\n";
	
	$news_url = "<a href=\"" . $itproject_url . "/";
	$news_url .= "newsView.php?id=" . $row['id'];
	$news_url .= "\">";
	echo $news_url;
	echo $row['subject'];
	echo "</a>";
	echo "</td>\n";
	

//	status (1 = done; 2 = ongoing; 3 = onhold ; 4 = cancelled)


	echo "</tr>\n";
}

 
pg_close($conn);


?>

            
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </tr>
</table>
</body>
</html>
