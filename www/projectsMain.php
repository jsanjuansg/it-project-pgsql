<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$today = mktime();

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Profiles Main</title>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Projects</td>
        </tr>
        <tr class="maintext">
          <td width="21%"><a href="projectsCreateNew.php">Create New Project</a></td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
            <tr>
              <td width="44%" class="menubar">Name</td>
              <td width="20%" class="menubar">Start Date</td>
              <td width="18%" class="menubar">End Date</td>
              <td width="18%" class="menubar">Status</td>
            </tr>
            <tr>
            

<?php

$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 


/*

projects
id
name
manager
start_date
end_date
actual_start_date
actual_end_date
status
privacy
description


*/

$sql = "SELECT id,name,start_date,end_date,status FROM projects";

//echo $sql;


$result = pg_query($conn, $sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $query;
    die($message);
}

while ( $row = pg_fetch_assoc($result) ) {

	$edate = strtotime($row['end_date']);
	$date_diff = time_duration($today, $edate);
	$days_diff = number_format($date_diff['allday'], 0);
	
	if ( $days_diff <= 0 )  {	
		$project_status = "PROJECT DONE";		
		echo "<tr class=\"projectdone\">\n";
		$css = "projectdone";
		
	}  elseif  ($days_diff <= 7 )  {
		$project_status = $days_diff;
		$project_status .= " days left before project deadline";
		echo "<tr class=\"projectdonenear\">\n";
		$css = "projectdonenear";
		
	}  else  {
		$project_status = $days_diff;
		$project_status .= " days left before project deadline";		
		echo "<tr>";
		$css = "projectongoing";
	}


	
	echo "<td>\n";
	$file_url = "<a href=\"" . $itproject_url . "/";
	$file_url .= "projectsView.php?id=" . $row['id'];
	$file_url .= "\"";
	$file_url .= "class=\"";
	$file_url .= $css;
	$file_url .= "\">";	
	echo $file_url;
	echo $row['name'];
	echo "</a>";
	echo "</td>\n";
	
	echo "<td>\n";
	echo $row['start_date'];
	echo "</td>\n";
	
	echo "<td>\n";
	echo $row['end_date'];
	echo "</td>\n";
	
//	status (1 = done; 2 = ongoing; 3 = onhold ; 4 = cancelled)

	echo "<td>\n";
	echo $project_status;
	echo "</td>\n";
	
	echo "</tr>\n";
	
}

 
pg_close($conn);


?>

            
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </tr>
</table>
</body>
</html>
