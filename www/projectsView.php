<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$projectid = trim($_GET['id']);


$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}

$username = $_SESSION['itp_username'];

/*

id
name
manager
start_date
end_date
actual_start_date
actual_end_date
status (1 = done; 2 = ongoing; 3 = onhold ; 4 = cancelled)
privacy ( 0 = private; 1 = public)
description

mysql_insert_id 

mktime(0,0,0,mon,day,year);

*/

$sql = sprintf("SELECT * FROM projects WHERE id = %d", $projectid);

//echo $sql;

$result = pg_query($conn, $sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

$num_rows = pg_num_rows($result);

if ( $num_rows < 1 )  {
	$projectname = "PROJECT ID NOT FOUND";
}  else  {
	$row = pg_fetch_assoc($result);
	$projectname = $row['name'];
	$projectdescription = $row['description'];
	$projectstartdate = $row['start_date'];
	$projectenddate = $row['end_date'];
}
pg_close($conn);


?>

<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Profiles Main</title>
<style type="text/css">
<!--
.style37 {font-family: Arial, Helvetica, sans-serif; font-size: 14; }
.style38 {font-size: 14}
-->
</style>
</head>

<body>
<form name="form1" method="post" action="projectsEditDelete.php">

<?php


echo "<input name=\"id\" type=\"hidden\" value=";
echo $projectid;
echo ">";


?>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Project Details</td>
        </tr>
        <tr class="maintext">
          <td width="21%">&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="70%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#CCCCCC" class="dialogbox">
            <tr>
              <td width="50%" class="style37"><p class="style37">Project Name: </p></td>
              <td width="50%">

<?php

	echo $projectname;


?>              </td>
            </tr>
            <tr>
              <td class="style37">Project Description:</td>
              <td>
              
<?php


	echo $projectdescription;
	

?>              </td>
            </tr>
            <tr>
              <td class="style37">Project Start Date: </td>
              <td>

<?php


echo $projectstartdate;


?>              </td>
            </tr>
            <tr>
              <td class="style37">Project End Date: </td>
              <td>

<?php


echo $projectenddate;


?>              </td>
            </tr>
            
            <tr>
              <td class="style37"><span class="style38"></span></td>
              <td><span class="style38"></span></td>
            </tr>
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">Project Members: </td>
            </tr>
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            
            
<?php

$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}



/*

projects_members
project_id
username

*/

$sql = "SELECT username FROM projects_members WHERE project_id=" . $projectid;

//echo $sql;


$result = pg_query($conn, $sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $query;
    die($message);
}

while ( $row = pg_fetch_assoc($result) ) {
	if ( $row['username'] != "administrator" )  {
		echo "<tr>\n";
		echo "<td colspan=\"2\">\n";
		echo $row['username'];
		echo "</td>\n";
		echo "</tr>\n";
	}
}

 
pg_close($conn);


?>



            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37"><label>
                  <div align="center">
                    <input name="editproject" type="submit" class="dialogboxbuttons" id="editproject" value="     EDIT PROJECT     " />
                     &nbsp;&nbsp;
                     <input name="deleteproject" type="submit" class="dialogboxbuttons" id="deleteproject" value="     DELETE PROJECT     " />
                  </div>
                </label></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
  </tr>
</table>
</form>
</body>
</html>
