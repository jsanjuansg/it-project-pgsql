<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$id = $_GET['id'];

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Forums Topic View</title>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Forums</td>
        </tr>
        <tr class="maintext">
          <td width="21%">
          
<?php
          

echo "<a href=\"forumsCreateNewPost.php?id=";
echo $id;
echo "\">";
		  
?>
		  
		  Create New Post on this Topic</a></td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
                
        <tr class="maintext">
          <td><table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
          
          
<?php

$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}


/*

id
projects_id
subject
body
date_created
privacy
author


*/
$sql = sprintf("SELECT id,subject,date_created,author,body FROM forums WHERE id = %d", $id);

//echo $sql;


$result = pg_query($conn, $sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

$row = pg_fetch_assoc($result);

 



?>
          
            <tr>
              <td width="20%" class="menubar">

<?php

echo $row['subject'];
echo "<br>";

echo "<p class=\"forumsubtopicheader\">";
echo "by: ";
echo $row['author'];
echo "<br>";
echo $row['date_created'];
echo "<br>";
echo "<a href=\"forumsTopicDelete1.php?id=";
echo $id;
echo "\">Delete this Topic</a>";
echo "</p>";


?>

              
              </td>
              </tr>
              
              
<?php

echo "<tr>";
echo "<td class=\"maintext\">";
echo $row['body'];
echo "</td>";
echo "</tr>";


?>
              
            <tr>
              <td class="maintext">&nbsp;</td>
              </tr>

<?php

/*

FORUMS_MESSAGES

id
projects_id
subject
body
date_created
privacy
author
forums_id

*/
$sql2 = sprintf("SELECT id,subject,body,date_created,author FROM forums_messages WHERE forums_id = %d", $id);


$result = pg_query($conn, $sql2);
if (!$result2) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql2;
    die($message);
}

while ( $row2 = mysql_fetch_array($result2, MYSQL_ASSOC) ) {
	echo "<tr>\n";
	
	echo "<td class=\"forumsubtopic\">\n";
	echo $row2['author'];
	echo "<br>";
	echo "<p class=\"forumsubtopicheader\">";
	echo $row2['date_created'];
	echo "<br>";
	echo "<a href=\"forumsPostDelete1.php?id=";
	echo $row2['id'];
	echo "\">Delete this Post</a>";
	echo "</p>";
	echo "</td>\n";
	
	echo "</tr>";
	
	echo "<tr>\n";
	echo "<td class=\"maintext\">\n";	
	echo $row2['body'];
	echo "</td>\n";
	echo "</tr>";	

}

mysql_free_result($result2);
pg_close($conn);

?>

            <tr>
              <td class="maintext">&nbsp;</td>
            </tr>
            
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </tr>
</table>
</body>
</html>
