<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$projectname = trim($_POST['projectname']);
$projectdescription = trim($_POST['projectdescription']);
$projectmembers = $_POST['projectmembers'];

$count_members = count($projectmembers);

$smonth = $_POST['smonth'];
$sday = $_POST['sday'];
$syear = $_POST['syear'];

$emonth = $_POST['emonth'];
$eday = $_POST['eday'];
$eyear = $_POST['eyear'];

$sdatestamp = mktime(0,0,0,$smonth,$sday,$syear);
$edatestamp = mktime(0,0,0,$emonth,$eday,$eyear);

$sdate = date('Y-m-d', $sdatestamp);
$edate = date('Y-m-d', $edatestamp);

if ( !$edatestamp )  {
	header('Location: index.phperrorInvalidDate.php');
	exit();
}

if ( !$sdatestamp )  {
	header('Location: index.phperrorInvalidDate.php');
	exit();
}

if ( $count_members < 1 )  {
	header('Location: index.phperrorNoMembers.php');
	exit();
}

if ( empty($projectname) )  {
	header('Location: index.phperrorFieldsMissing.php');
	exit();
}


$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}

$projectname = pg_escape_string($projectname);
$projectdescription = pg_escape_string($projectdescription);
$username = $_SESSION['itp_username'];
$status = 2;
$privacy = 1;

/*

id
name
manager
start_date
end_date
actual_start_date
actual_end_date
status (1 = done; 2 = ongoing; 3 = onhold ; 4 = cancelled)
privacy ( 0 = private; 1 = public)
description

mysql_insert_id 

mktime(0,0,0,mon,day,year);

*/

$sql = sprintf("INSERT INTO projects (id,name,manager,start_date,end_date,actual_start_date,actual_end_date,status,privacy,description) VALUES(null,'%s','%s','%s', '%s', '%s', '%s', %d, %d, '%s')", $projectname,$username,$sdate,$edate,$sdate,$edate,$status,$privacy,$projectdescription);

//echo $sql;

$result = pg_query($conn, $sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $query;
    die($message);
}
$project_id = mysql_insert_id();

/*

projects_members
project_id
username

*/

$i = 0;

//echo $count_members;
//echo "\n\n";

while ( $count_members > $i )  {

	$sql2 = sprintf("INSERT INTO projects_members(project_id,username) VALUES(%d,'%s')", $project_id, $projectmembers[$i]);	
	$result = pg_query($conn, $sql2);

	if (!$result2) {
		$message = "SQL: " . $sql2 . "\n\n";
    	$message  .= 'Invalid query: ' . $conn->error . "\n";
    	$message .= 'Whole query: ' . $query;
		die($message);
	}
	$i++;
	
}

pg_close($conn);


?>

<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Profiles Main</title>
<style type="text/css">
<!--
.style37 {font-family: Arial, Helvetica, sans-serif; font-size: 14; }
.style38 {font-size: 14}
-->
</style>
<script language="JavaScript" src="itproject.js">
</script>
</head>

<body onLoad="setTimeout('projectsredirect()', 3000)">
<form name="form1" method="post" action="projectDoCreate.php">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">New Project Created</td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td width="21%">You will now be redirected to the Projects Page in 3 seconds. If not, click <a href="projectsMain.php">here</a>
          </td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="70%" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#CCCCCC">
            <tr>
              <td width="50%" class="style37"><p class="style37">Project Name: </p></td>
              <td width="50%">

<?php

	echo $projectname;


?>              </td>
            </tr>
            <tr>
              <td class="style37">Project Description:</td>
              <td>
              
<?php


	echo $projectdescription;
	

?>              </td>
            </tr>
            <tr>
              <td class="style37">Project Start Date: *</td>
              <td>

<?php


echo GenerateSelectMonths("smonth",1);
echo GenerateSelectDays("sday", 1, 31, 1);
echo GenerateSelectYears("syear", 2010, 2015, 2010);



?>              </td>
            </tr>
            <tr>
              <td class="style37">Project End Date: *</td>
              <td>

<?php


echo GenerateSelectMonths("emonth",1);
echo GenerateSelectDays("eday", 1, 31, 1);
echo GenerateSelectYears("eyear", 2010, 2015, 2010);



?>              </td>
            </tr>
            
            <tr>
              <td class="style37"><span class="style38"></span></td>
              <td><span class="style38"></span></td>
            </tr>
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">Project Members: *</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            
            
<?php

$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}



/*

username
firstname
lastname
email
position_id
group_id
password

*/

$sql = "SELECT username,firstname,lastname FROM users";

//echo $sql;


$result = pg_query($conn, $sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $query;
    die($message);
}

while ( $row = pg_fetch_assoc($result) ) {
	if ( $row['username'] != "administrator" )  {
		echo "<tr>\n";
		echo "<td colspan=\"2\">\n";
		echo "<input type=\"checkbox\" name=\"projectmembers[]\" value=\"" . $row['username'] . "\">\n";
		echo $row['firstname'] . " " . $row['lastname'];	
		echo "</td>\n";
		echo "</tr>\n";
	}
}

 
pg_close($conn);


?>



            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">* required fields</td>
            </tr>
            <tr>
              <td colspan="2" class="style37"><label>
                  <div align="center">
                    <input type="submit" name="createproject" id="createproject" value="     CREATE PROJECT     " />
                  </div>
                </label></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
  </tr>
</table>
</form>
</body>
</html>
