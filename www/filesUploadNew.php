<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Files Upload</title>
<style type="text/css">
<!--
.style37 {font-family: Arial, Helvetica, sans-serif; font-size: 14; }
.style38 {font-size: 14}
-->
</style>
</head>

<body>
<form name="form1" method="post" action="filesDoUpload.php" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Projects : Upload File</td>
        </tr>
        <tr class="maintext">
          <td width="21%">&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="70%" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#CCCCCC" class="dialogbox">
            <tr>
              <td width="50%" class="style37"><p class="style37">File: *</p></td>
              <td width="50%"><span class="style38">
                <label>
				<input name="file" type="file" size="40">
                </label>
              </span></td>
            </tr>
            <tr>
              <td class="style37">Description: *</td>
              <td><span class="style38">
                <label>
                <textarea name="description" cols="45" rows="5" tabindex="2" id="description"></textarea>
                </label>
              </span></td>
            </tr>
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">* required fields</td>
            </tr>
            <tr>
              <td colspan="2" class="style37"><label>
                  <div align="center">
                    <input name="createproject" type="submit" class="dialogboxbuttons" id="createproject" value="     UPLOAD FILE     " />
                  </div>
                </label></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
  </tr>
</table>
</form>
</body>
</html>
