<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$projectid = trim($_POST['id']);
$editproject = trim($_POST['editproject']);
$deleteproject = trim($_POST['deleteproject']);


if ( !empty($editproject) )  {
//	echo "edit project";
	$url = "Location: index.phpprojectsEditMain.php?id=";
	$url .= $projectid;	
	header($url);
	
}  elseif ( !empty($deleteproject) )  {
//	echo "delete project";
	$url = "Location: index.phpprojectsDoDelete.php?id=";
	$url .= $projectid;	
	header($url);
}


?>
