<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}


$description = trim($_POST['description']);
$privacy = 1;
$project_id = 0;

$smonth = $_POST['smonth'];
$sday = $_POST['sday'];
$syear = $_POST['syear'];
$hour = $_POST['hour'];
$minutes = $_POST['minutes'];
$day = $_POST['day'];
$secs = 0;

if ( $day == "PM" )  {
	if ( $hour == 12 )  {
		$hour = 23;
	}  else  {
		$hour = $hour + 12;
	}
}

$sdatestamp = mktime($hour,$minutes,$secs,$smonth,$sday,$syear);
$sdate = date('Y-m-d H:i:s', $sdatestamp);

if ( !$sdatestamp )  {
	header('Location: index.phperrorInvalidDate.php');
}

if ( empty($description) )  {
	header('Location: index.phperrorFieldsMissing.php');
}


$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}

$subject = pg_escape_string($subject);
$description = pg_escape_string($description);
$username = $_SESSION['itp_username'];
$status = 2;
$privacy = 1;

/*

id
date
username
project_id
text
privacy ( 0 = private; 1 = public)

mysql_insert_id 

mktime(0,0,0,mon,day,year);

*/

$sql = sprintf("INSERT INTO calendar (id,date,username,project_id,text,privacy) VALUES(null,'%s','%s', %d, '%s', %d)", $sdate,$username,$project_id,$description,$privacy);

//echo $sql;

$result = pg_query($conn, $sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}
$calendar_id = mysql_insert_id();


pg_close($conn);


?>

<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Calendar Add New </title>
<style type="text/css">
<!--
.style37 {font-family: Arial, Helvetica, sans-serif; font-size: 14; }
.style38 {font-size: 14}
-->
</style>
<script language="JavaScript" src="itproject.js">
</script>
</head>

<body onLoad="setTimeout('eventsredirect()', 3000)">
<form name="form1" method="post" action="projectDoCreate.php">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">New Event Created</td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td width="21%">You will now be redirected to the Events Page in 3 seconds. If not, click <a href="eventsMain.php">here</a> </td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="70%" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#CCCCCC" class="dialogbox">
            <tr>
              <td class="style37">Description:</td>
              <td>
              
<?php


	echo $description;
	

?>              </td>
            </tr>
            <tr>
              <td class="style37">Date:</td>
              <td>

<?php


echo $sdate;


?>              </td>
            </tr>
                       
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
  </tr>
</table>
</form>
</body>
</html>
