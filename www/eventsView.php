<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$eventid = trim($_GET['id']);


$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}

$username = $_SESSION['itp_username'];

/*

id
date
username
project_id
text
privacy


*/

$sql = sprintf("SELECT * FROM calendar WHERE id = %d", $eventid);

//echo $sql;

$result = pg_query($conn, $sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

$num_rows = pg_num_rows($result);

if ( $num_rows < 1 )  {
	$projectname = "EVENT ID NOT FOUND";
}  else  {
	$row = pg_fetch_assoc($result);
	$eventauthor = $row['username'];
	$eventdate = $row['date'];
	$eventbody = $row['text'];
}
pg_close($conn);


?>

<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Events Main</title>
<style type="text/css">
<!--
.style37 {font-family: Arial, Helvetica, sans-serif; font-size: 14; }
.style38 {font-size: 14}
-->
</style>
</head>

<body>
<form name="form1" method="post" action="eventsDoDelete.php">

<?php


echo "<input name=\"id\" type=\"hidden\" value=";
echo $eventid;
echo ">";


?>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Event Details</td>
        </tr>
        <tr class="maintext">
          <td width="21%">&nbsp;</td>
        </tr>
        
        <tr class="maintext">
          <td><table width="70%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#CCCCCC" class="dialogbox">
            <tr>
              <td width="50%" class="style37"><p class="style37">Event Author: </p></td>
              <td width="50%">

<?php

	echo $eventauthor;


?>              </td>
            </tr>
            <tr>
              <td class="style37">Event Date:</td>
              <td>
              
<?php


	echo $eventdate;
	

?>              </td>
            </tr>
            
            <tr>
              <td class="style37">Event Description:</td>
              <td>&nbsp;</td>
            </tr>
            
            <tr>
              <td colspan="2" class="style37"><span class="style38"></span><span class="style38">
<?php


echo $eventbody;


?>
              </span></td>
              </tr>
            
            
            

            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37"><label>
                  <div align="center">                    
                     <input name="deleteproject" type="submit" class="dialogboxbuttons" id="deleteproject" value="     DELETE THIS EVENT     " />
                  </div>
                </label></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
  </tr>
</table>
</form>
</body>
</html>
