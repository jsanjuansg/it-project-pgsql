<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Profiles Main</title>
<style type="text/css">
<!--
.style37 {font-family: Arial, Helvetica, sans-serif; font-size: 14; }
.style38 {font-size: 14}
-->
</style>
</head>

<body>
<form name="form1" method="post" action="calendarDoCreateNew.php">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Events: Create New Entry</td>
        </tr>
        <tr class="maintext">
          <td width="21%">&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="70%" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#CCCCCC" class="dialogbox">
            <tr>
              <td class="style37">Description:</td>
              <td><span class="style38">
                <label>
                <textarea name="description" id="description" cols="45" rows="5" tabindex="2"></textarea>
                </label>
              </span></td>
            </tr>
            <tr>
              <td class="style37">Date: *</td>
              <td>

<?php


echo GenerateSelectMonths("smonth",1);
echo GenerateSelectDays("sday", 1, 31, 1);
echo GenerateSelectYears("syear", 2010, 2015, 2010);



?>				</td>
            </tr>

            <tr>
              <td class="style37">Time: *</td>
              <td class="style37">
                Hour 
                <select name="hour">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12" selected>12</option>
                </select> 
                Minutes 
                <select name="minutes">
                  <option value="0" selected>0</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                  <option value="13">13</option>
                  <option value="14">14</option>
                  <option value="15">15</option>
                  <option value="16">16</option>
                  <option value="17">17</option>
                  <option value="18">18</option>
                  <option value="19">19</option>
                  <option value="20">20</option>
                  <option value="21">21</option>
                  <option value="22">22</option>
                  <option value="23">23</option>
                  <option value="24">24</option>
                  <option value="25">25</option>
                  <option value="26">26</option>
                  <option value="27">27</option>
                  <option value="28">28</option>
                  <option value="29">29</option>
                  <option value="30">30</option>
                  <option value="31">31</option>
                  <option value="32">32</option>
                  <option value="33">33</option>
                  <option value="34">34</option>
                  <option value="35">35</option>
                  <option value="36">36</option>
                  <option value="37">37</option>
                  <option value="38">38</option>
                  <option value="39">39</option>
                  <option value="40">40</option>
                  <option value="41">41</option>
                  <option value="42">42</option>
                  <option value="43">43</option>
                  <option value="44">44</option>
                  <option value="45">45</option>
                  <option value="46">46</option>
                  <option value="47">47</option>
                  <option value="48">48</option>
                  <option value="49">49</option>
                  <option value="50">50</option>
                  <option value="51">51</option>
                  <option value="52">52</option>
                  <option value="53">53</option>
                  <option value="54">54</option>
                  <option value="55">55</option>
                  <option value="56">56</option>
                  <option value="57">57</option>
                  <option value="58">58</option>
                  <option value="59">59</option>
                </select> 
                <select name="day">
                  <option value="AM" selected>AM</option>
                  <option value="PM">PM</option>
                </select>
              </td>
            </tr>
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">* required fields</td>
            </tr>
            <tr>
              <td colspan="2" class="style37"><label>
                  <div align="center">
                    <input name="createproject" type="submit" class="dialogboxbuttons" id="createproject" value="     CREATE NEW EVENT        " />
                  </div>
                </label></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
  </tr>
</table>
</form>
</body>
</html>
