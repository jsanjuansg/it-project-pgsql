<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Forums Main</title>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Forums</td>
        </tr>
        <tr class="maintext">
          <td width="21%"><a href="forumsCreateNewTopic.php">Create New Forum Topic</a></td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
            <tr>
              <td width="20%" class="menubar">Topic</td>
              <td width="20%" class="menubar">Author</td>
              <td width="10%" class="menubar">Date</td>
            </tr>
            <tr>
            

<?php

$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 
 


/*

id
projects_id
subject
body
date_created
privacy
author


*/
$sql = "SELECT id,subject,date_created,author FROM forums";

//echo $sql;


$result = pg_query($conn, $sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

while ( $row = pg_fetch_assoc($result) ) {
	echo "<tr>\n";
	
	echo "<td>\n";
	$forum_url = "<a href=\"" . $itproject_url . "/";
	$forum_url .= "forumsTopicView.php?id=" . $row['id'];
	$forum_url .= "\">";
	echo $forum_url;
	
	echo $row['subject'];
	
	echo "</a>";
	echo "</td>\n";

	echo "<td>\n";
	echo $row['author'];
	echo "</td>\n";
	
	echo "<td>\n";
	echo $row['date_created'];
	echo "</td>\n";
	
	echo "</tr>\n";
}

 
pg_close($conn);


?>

            
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </tr>
</table>
</body>
</html>
