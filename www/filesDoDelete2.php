<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}


$files = $_POST['fileselect'];
$fcount = count($files);

if ( $fcount < 1 )  {
	header('Location: index.phpfilesMain.php');
}



?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Files Main</title>
<script language="JavaScript" src="itproject.js">
</script>
</head>

<body onLoad="setTimeout('filesredirect()', 3000)">
<form name="form1" method="post" action="filesDoDelete2.php">

<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Files Deleted</td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td>You will now be redirected to the Files Main Page in 3 seconds. If not, click <a href="filesMain.php">here</a> </td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
            <tr>
              <td width="6%" class="menubar">&nbsp;</td>
              <td width="44%" class="menubar">Filename</td>
              <td width="29%" class="menubar">Description</td>
              <td width="21%" class="menubar">Date</td>
            </tr>
            <tr>
            

<?php

$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}


/*

name
username
project_id
fdate
privacy
description

*/

//echo $sql;

for ( $i = 0; $i < $fcount; ++$i )  {

	$sql = "DELETE FROM files WHERE name = '";
	$sql .= $files[$i];
	$sql .= "'";
	$result = pg_query($conn, $sql);
	if (!$result) {
    	$message  = 'Invalid query: ' . $conn->error . "\n";
    	$message .= 'Whole query: ' . $sql;
    	die($message);
	}
	
	$uploaddir = '/home/itprojec/public_html/files/';
	$uploadfile = $uploaddir . $files[$i];
	unlink($uploadfile);
	
	echo "<tr>\n";
	
	echo "<td colspan=\"3\">";
	echo "Files Deleted";
	echo "</td>";
	
	echo "</tr>\n";


}

pg_close($conn);



?>
            </tr>
            <tr>
              <td width="6%" class="menubar">&nbsp;</td>
              <td width="44%" class="menubar">&nbsp;</td>
              <td width="29%" class="menubar">&nbsp;</td>
              <td width="21%" class="menubar">&nbsp;</td>
            </tr>
            
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </tr>
</table>
</form>
</body>
</html>
