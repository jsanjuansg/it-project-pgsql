<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$newsid = trim($_GET['id']);


$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}

$username = $_SESSION['itp_username'];

/*

id
date
username
text
project_id
privacy ( 0 = private; 1 = public)
subject

mysql_insert_id 

*/
$sql = sprintf("SELECT * FROM news WHERE id = %d", $newsid);

//echo $sql;

$result = pg_query($conn, $sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

$num_rows = pg_num_rows($result);

if ( $num_rows < 1 )  {
	$projectname = "NEWS ID NOT FOUND";
}  else  {
	$row = pg_fetch_assoc($result);
	$newssubject = $row['subject'];
	$newsauthor = $row['username'];
	$newsdate = $row['date'];
	$newsbody = $row['text'];
}
pg_close($conn);


?>

<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Profiles Main</title>
<style type="text/css">
<!--
.style37 {font-family: Arial, Helvetica, sans-serif; font-size: 14; }
.style38 {font-size: 14}
-->
</style>
</head>

<body>
<form name="form1" method="post" action="newsDoDelete.php">

<?php


echo "<input name=\"id\" type=\"hidden\" value=";
echo $newsid;
echo ">";


?>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">News Details</td>
        </tr>
        
        <tr class="maintext">
          <td width="21%">&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="70%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#CCCCCC" class="dialogbox">
            <tr>
              <td width="50%" class="style37"><p class="style37">News Author: </p></td>
              <td width="50%">

<?php

	echo $newsauthor;


?>              </td>
            </tr>
            <tr>
              <td class="style37">News Date:</td>
              <td>
              
<?php


	echo $newsdate;
	

?>              </td>
            </tr>
            <tr>
              <td class="style37">News Subject: </td>
              <td>

<?php


echo $newssubject;


?>              </td>
            </tr>
            <tr>
              <td class="style37">News:</td>
              <td>&nbsp;</td>
            </tr>
            
            <tr>
              <td colspan="2" class="style37"><span class="style38"></span><span class="style38">
<?php


echo $newsbody;


?>
              </span></td>
              </tr>
            
            
            

            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">
              
<?php


if ( $_SESSION['itp_position'] == 1 )  {          
	echo "<div align=\"center\">";
	echo "<input name=\"deleteproject\" type=\"submit\" class=\"dialogboxbuttons\" value=\"     DELETE THIS NEWS     \" />";
	echo "</div>";
	
}  elseif ( $_SESSION['itp_username'] == "administrator" )  {
	echo "<div align=\"center\">";
	echo "<input name=\"deleteproject\" type=\"submit\" class=\"dialogboxbuttons\" value=\"     DELETE THIS NEWS     \" />";
	echo "</div>";

	
}  else  {
	echo "&nbsp;";
}

              
                  
				  
				  
?>
				  
               </td>
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
  </tr>
</table>
</form>
</body>
</html>
