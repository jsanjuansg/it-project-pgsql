<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Help Main</title>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain"><img src="img/about.gif" width="48" height="48"> About</td>
        </tr>
        <tr class="maintext">
          <td width="21%"></td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="95%" border="0" align="center" cellpadding="5" cellspacing="0" class="dialogbox">
            
            <tr>
              <td width="44%" class="maintext"><ul><li>The  system will be a web-based multi-user application that will provide a portal to  the essential information needed in the projects of an IT Deparment.&nbsp; It will provide assistance to an IT  department of a company with regards to project scheduling, monitoring,  tracking, information organization and information sharing.&nbsp;</li>
                </ul></td>             
            </tr>                                
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </tr>
</table>
</body>
</html>
