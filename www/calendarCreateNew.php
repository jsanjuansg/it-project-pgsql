<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Profiles Main</title>
<style type="text/css">
<!--
.style37 {font-family: Arial, Helvetica, sans-serif; font-size: 14; }
.style38 {font-size: 14}
-->
</style>
</head>

<body>
<form name="form1" method="post" action="calendarDoCreateNew.php">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Calendar: Create New Entry</td>
        </tr>
        <tr class="maintext">
          <td width="21%">&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="70%" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#CCCCCC">
            <tr>
              <td class="style37">Description:</td>
              <td><span class="style38">
                <label>
                <textarea name="description" id="description" cols="45" rows="5" tabindex="2"></textarea>
                </label>
              </span></td>
            </tr>
            <tr>
              <td class="style37">Date: *</td>
              <td>

<?php


echo GenerateSelectMonths("smonth",1);
echo GenerateSelectDays("sday", 1, 31, 1);
echo GenerateSelectYears("syear", 2010, 2015, 2010);



?>              

				</td>
            </tr>

            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">* required fields</td>
            </tr>
            <tr>
              <td colspan="2" class="style37"><label>
                  <div align="center">
                    <input type="submit" name="createproject" id="createproject" value="     CREATE CALENDAR ENTRY     " />
                  </div>
                </label></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
  </tr>
</table>
</form>
</body>
</html>
