<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$projectid = trim($_GET['id']);


$conn = pg_connect( "$dbhost $dbport $dbase $dbuser $dbpassword");
if (!$conn) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}

$sql1 = sprintf("DELETE FROM projects WHERE id = %d", $projectid);

//echo $sql1;

$result1 = mysql_query($sql1, $link);
if (!$result1) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql1;
    die($message);
}

$sql2 = sprintf("DELETE FROM projects_members WHERE project_id = %d", $projectid);

//echo $sql1;

$result = pg_query($conn, $sql2);
if (!$result1) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql2;
    die($message);
}


pg_close($conn);


?>

<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Profiles Main</title>
<style type="text/css">
<!--
.style37 {font-family: Arial, Helvetica, sans-serif; font-size: 14; }
.style38 {font-size: 14}
-->
</style>
<script language="JavaScript" src="itproject.js">
</script>
</head>

<body onLoad="setTimeout('projectsredirect()', 3000)">
<form name="form1" method="post" action="projectsEditDelete.php">

<?php


echo "<input name=\"id\" type=\"hidden\" value=";
echo $projectid;
echo ">";


?>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Project Deleted</td>
        </tr>
        <tr class="maintext">
          <td width="21%">&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td>You will now be redirected to the Projects Page in 3 seconds. If not, click <a href="projectsMain.php">here</a> </td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
      </table>
      <p>&nbsp;</p>
  </tr>
</table>
</form>
</body>
</html>
